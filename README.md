# tizen-saolytics

The official Saolytics Insights SDK for Samsung Tizen applications

## SDK Overview

Saolytics Insights supports applications built for Samsung Tizen TVs using JavaScript and Tizen's [AVPlay API](https://developer.samsung.com/tv/develop/api-references/samsung-product-api-references/avplay-api). For integration instructions in your application, see our documentation here: https://docs.saolytics.com/saolytics_insights/integration_guide_tizen.html.

If you run into any issues, don't hesitate to get in touch by creating an issue in this repo, or [reaching out to us directly](mailto:support@saola.io).

## Application Support

Applications for Tizen-based TVs can be written in C++, JavaScript, and Microsoft .NET. Currently, Saolytics Insights only supports applications written in JavaScript. As such, when you craete your application using the Tizen Studio, you must choose "Web Application".

Get in touch if you need Saolytics to integrate with your native applications written in other languages on Samsung TVs.

## Directory Layout

```
  - app
    - tizenPlayer - Sample Tizen application using AVPlay
  - scripts - deployment scripts
  - src
    - index.js - Saolytics integration
    - entry.js - packaging file
```

## Sample Application

A sample demo application is provided in the `app/tizenPlayer` directory, which implements a player and basic player controls for play, stop, pause, resume, fast-forward, and rewind. This sample application is also integrated with Saolytics Insights, showing the integration steps necessary. See our [integration guide](https://docs.saolytics.com/saolytics_insights/integration_guide_tizen.html) for more detailed information on integration.

## Saolytics Insights Integration

The Saolytics Insights integration, which uses `saolytics-embed` (the core Saolytics JavaScript SDK), is comprised of the scripts within the `src` directory. The integration itself is written usein ES6 and various other dependenciees, managed via `yarn`. This is then compiled and minified using Webpack, and hosted via https://sdk.saolytics.com/tizen/[major_version]/tizen-saolytics.js.

* http://localhost:8080/index.html
* http://localhost:8080/ads.html

## Release Notes

### Current Release

#### v0.1.0

  - Initial SDK released
